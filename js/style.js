// получим все табки
let tabs = document.querySelectorAll(".tabs-title");
let tabContents = document.querySelector(".tabs-content").children;

//прячем все блоки с контентом
function hideTab() {
  for (let index = 0; index < tabContents.length; index++) {
    const element = tabContents[index];
    element.style.display = "none";
  }
}

// выбираем нужный блок с контентом
function showTab(i = 0) {
  tabContents[i].style.display = "block";
}

//вызываем функцыи
hideTab();
showTab();

//распознаём клик по табкам

tabs.forEach(function (tab, i) {
  tab.addEventListener("click", function (event) {
    hideTab();
    tabs.forEach(function (tab) {
      if (event.target === tab) {
        tab.classList.add("active");
      } else {
        tab.classList.remove("active");
      }
    });
    showTab(i);
  });
});
